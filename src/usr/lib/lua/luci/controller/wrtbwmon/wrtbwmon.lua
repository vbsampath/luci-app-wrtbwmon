module("luci.controller.wrtbwmon.wrtbwmon", package.seeall)

local template = require("luci.template")
local i18n = require("luci.i18n")



function string:split( inSplitPattern, outResults )
  if not outResults then
    outResults = { }
  end
  local theStart = 1
  local theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
  while theSplitStart do
    table.insert( outResults, string.sub( self, theStart, theSplitStart-1 ) )
    theStart = theSplitEnd + 1
    theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
  end
  table.insert( outResults, string.sub( self, theStart ) )
  return outResults
end

function index()
	entry({"admin", "services", "wrtbwmon"}, call("action_index"), "Wrtbwmon", 10).dependent=false
	entry({"admin", "services", "wrtbwmon", "getData"}, call("action_getData"))
end

function action_getData()
	
	local macTable = {}
	local dataTable = {}
	local macs = io.open("/root/MAC_addresses")
	
	--reading mac address file
	counter = 1
	if macs then
		while true do
			local line = macs:read("*l")
			
			if not line then
				break
			end

			local myTable = line:split(",")
			
			macTable[counter] = myTable
			counter = counter + 1
			
		end
		macs:close()
	end
	
	dataTable["macs"] = macTable
	
	luci.http.prepare_content("application/json")
	luci.http.write_json(dataTable)
	
end

function action_index()

	local usageTable = {}
	local contentTable = {}
	local counter = 0
	local usage = io.open("/tmp/usage.db")
	local removeEmptyEntries = false
	local emptyEntryIndex = 6
	local emptyColumnNumber = 0
	
	--reading usage db file
	if usage then
		while true do
			local line = usage:read("*l")
			
			if not line then
				break
			end

			local myTable = line:split(",")
			
			if(removeEmptyEntries) then
				emptyColumnNumber = tonumber(myTable[emptyEntryIndex])
				if (emptyColumnNumber ~= 0) then
					usageTable[counter] = myTable
					counter = counter + 1
				end
			else
				usageTable[counter] = myTable
				counter = counter + 1
			end
		end
		usage:close()
	end
	
	
	--creating readable headers
	local usageTableHeaders = {}
	table.insert(usageTableHeaders, {"#mac", "MAC Address"})
	table.insert(usageTableHeaders, {"ip", "IP Address"})
	table.insert(usageTableHeaders, {"iface", "Interface"})
	table.insert(usageTableHeaders, {"in", "Inwards"})
	table.insert(usageTableHeaders, {"out", "Outwards"})
	table.insert(usageTableHeaders, {"total", "Total"})
	table.insert(usageTableHeaders, {"first_date", "First Seen"})
	table.insert(usageTableHeaders, {"last_date", "Last Seen"})
	
	--setting data for output
	contentTable["usageTable"] = usageTable
	contentTable["usageTableCount"] = table.maxn(usageTable)
	contentTable["usageTableHeaders"] = usageTableHeaders
	contentTable["usageTableColumnCount"] = table.maxn(usageTable[0])
	
	template.render("wrtbwmon/index", {title = i18n.translate("Usage Details"), content = contentTable})
end