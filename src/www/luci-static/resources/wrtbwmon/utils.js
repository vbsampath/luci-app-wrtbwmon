// Epochs
const epochs = [
    ['year', 31536000],
    ['month', 2592000],
    ['day', 86400],
    ['hour', 3600],
    ['minute', 60],
    ['second', 1]
];


// Get duration
const getDuration = (timeAgoInSeconds) => {
    for (let [name, seconds] of epochs) {
        const interval = Math.floor(timeAgoInSeconds / seconds);

        if (interval >= 1) {
            return {
                interval: interval,
                epoch: name
            };
        }
    }
};

const convertDateToUnixFormat = (dateString) => {
    var matches = dateString.match(/(\d+)-(\d+)-(\d+)_(\d+):(\d+):(\d+)/);
    var date, month, year, hours, minutes, seconds, dateInUnix;
    date = matches[1];
    month = matches[2];
    year = matches[3];
    hours = matches[4];
    minutes = matches[5];
    seconds = matches[6];
    
    dateInUnix = year + "-" + month + "-" + date + "T" + hours + ":" + minutes + ":" + seconds;
	return dateInUnix;
};

function getHumanReadableSize(size) {
	var prefix=new Array("","k","M","G","T","P","E","Z"); 
	var base=1000;
	var pos=0;
	while (size>base) {
		size/=base; pos++;
	}
	if (pos > 2) precision=1000; else precision = 1;
	return (Math.round(size*precision)/precision)+' '+prefix[pos];
};


// Calculate
const timeAgo = (date) => {
    const timeAgoInSeconds = Math.floor((new Date() - new Date(date)) / 1000);
    const {interval, epoch} = getDuration(timeAgoInSeconds);
    const suffix = interval === 1 ? '' : 's';

    return `${interval} ${epoch}${suffix} ago`;
};